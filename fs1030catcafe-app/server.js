import express from 'express'
import cors from 'cors'
import connection from './src/database/connection'


const app = express()

app.use(cors())

app.use(express.json())

app.get('/api', (req,res) => {
    
    return res.send('hello world!')

})

app.get('/api/cats', (req,res) => {

    connection.query('SELECT * FROM cats', function (error, results, fields) {

        if (error) throw error

        return res.status(200).send(results)
    })
})

app.listen(3001, () => console.log(`API server ready on http://localhost:3001`))