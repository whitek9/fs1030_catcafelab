import CatProfile from './components/catProfile'
import './App.css';

function App() {
  return (
    <div>
        <CatProfile/>
    </div>
  );
}

export default App;
