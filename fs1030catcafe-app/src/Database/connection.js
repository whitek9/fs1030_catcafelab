import mysql from 'mysql'

const connection = mysql.createConnection( {
    host: 'localhost',
    user: 'root',
    password: 'password',
    database: 'kellycatcafe'
})

connection.connect(function(err) {
    if (err) {
        console.error('error connecting: ' + err.stack)
        return
    }

    console.log('Database Connected')
})


export default connection