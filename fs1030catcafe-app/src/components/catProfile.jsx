import React, { useEffect, useState } from 'react';


const CatProfile = () => {

    const [cats, setCats] = useState([])

    useEffect(() => {
        const getCats = async () => {
            const response = await fetch('http://localhost:3001/api/cats', {
                method: 'GET',
                headers: {
                    // 'Authorization': `Bearer ${token}`
                }
            })
            const data = await response.json()
            setCats(data)
        }
        getCats()
    })

    return (
       <div>
            {cats.map((cat) =>
                <div key={cat.id}>
                    <p>{cat.name}</p>
                    <img 
                        src={cat.images} 
                        alt={cat.name} 
                        width='300px' 
                        height='auto'
                    />
                </div>
            )}
       </div>
    );
}
 
export default CatProfile;